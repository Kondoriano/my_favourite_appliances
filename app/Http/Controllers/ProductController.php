<?php

namespace App\Http\Controllers;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller{

    CONST PRODUCT_TABLE = 'products';

    public function create($params, $category_id, $brand = null){
        $product = new Product;
        $product->name = $params["name"];
        $product->price = $params["price"];
        $product->description = $params["description"];
        $product->image_name = $params["image_name"];

        if($brand != null){
            $product->brand()->associate($brand->id);
        }

        $product->category()->associate($category_id);

        $product->save();

        return $product;
    }

    public function update($product, $params){
        $product->price = $params["price"];
        $product->description = $params["description"];
        $product->image_name = $params["image_name"];
        $product->updated_at = date("Y-m-d H:i:s");

        $product->save();
    }

    public function getbyCategory($id){
        $products = Category::find($id)->products;

        return $products;
    }

    public function getProductTableName(){
        return self::PRODUCT_TABLE;
    }

    public function findOneByField($field, $value){
        return Product::where($field, $value)->first();
    }

    public function getProducts(Request $request){
        $subcategory = Subcategory::where('id', $request->subcategory_id)->first();

        if(!empty($request->order_by) && !empty($request->order_direction)){
            $order_by = $request->order_by;
            $order_direction = $request->order_direction;
        } else{
            $order_by = 'name';
            $order_direction = 'asc';
        }

        $products = $subcategory->products()->orderBy($order_by, $order_direction)->get();

        return view('partials._products_catalog')->with(['products' => $products, 'subcategory_name' => $subcategory->name]);
    }
}
