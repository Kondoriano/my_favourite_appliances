<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    CONST CATEGORY_TABLE = 'categories';

    public function create($params = array()){
        $category = new Category;
        $category->name = $params['name'];

        $category->save();

        return $category;
    }

    public function getCategoryTableName(){
        return self::CATEGORY_TABLE;
    }

    public function findOneByField($field, $value){
        return Category::where($field, $value)->first();
    }

    public function findAll(){
        return Category::find()->all();
    }
}
