<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    CONST BRAND_TABLE = 'brands';

    public function create($params){
        $brand = new Brand;

        $brand->name = $params["name"];
        $brand->save();

        return $brand;
    }

    public function getBrandTableName(){
        return self::BRAND_TABLE;
    }

    public function findOneByField($field, $value){
        return Brand::where($field, $value)->first();
    }
}
