<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;

class WebCrawlerController extends Controller
{
    public function crawlProduct(Request $request){
        set_time_limit(0);

        $product_ctrl   = new ProductController();
        $subcategory_ctrl = new SubcategoryController();

        //Getting general category, each category will have a multiple subcategories
        if(!empty($request->url)){
            $url = $request->url;
        } else{
            $url = "https://www.appliancesdelivered.ie/search/small-appliances"; //Value by default
        }
        $content = file_get_contents($url);
        $crawler = new Crawler($content);

        //region Category
        $category_filter_name = $crawler->filter('div[class="main"] div[class="row"] ul[class="breadcrumb"] li[class="active"] a')->text();
        $category = $this->crawlCategory($category_filter_name);

        //endregion

        $subcategory_filter_links = $crawler->filter('div[id="desktop-search-filter-refine-filter-categories"] div[class="control-list-item"]');

        foreach ($subcategory_filter_links as $subcategory_filter_link){
            $subcategory_crawler = new Crawler($subcategory_filter_link);
            $subcategory_link = $subcategory_crawler->filter('input')->attr('data-url');

            $no_more_results = true;
            $page = 1;

            while ($no_more_results){ //We're going to go through all the available pages
                $subcategory_content = file_get_contents($subcategory_link . "?page=$page");
                $new_crawler = new Crawler($subcategory_content);
                $check_results = trim($new_crawler->filter('div[class="search-results col-md-9"] div[class="row"] div[class="col-xs-12"]')->text());

                if($check_results == "No results found"){ //To check if there are no more results
                    $no_more_results = false;
                } else{
                    $subcategory_name = trim( $new_crawler->filter('h1[class="result-list-title"]')->text());
                    $isset_subcategory = $subcategory_ctrl->findOneByField('name', $subcategory_name);
                    if($isset_subcategory === null){

                        $subcategory = $subcategory_ctrl->create(array('name' => $subcategory_name), $category);
                    } else{
                        $subcategory = $isset_subcategory;
                    }

                    $product_filters = $new_crawler->filter('div[class="search-results-product row"]');

                    foreach ($product_filters as $product_filter){
                        $product_crawler = new Crawler($product_filter);
                        $product_name = trim(htmlentities($product_crawler->filter('div[class="product-description col-xs-8 col-sm-8"] h4')->text()));
                        $product_price = str_replace(array('€', '$',','),'',$product_crawler->filter('div[class="product-description col-xs-8 col-sm-8"] h3[class="section-title"]')->text());
                        $product_description_html = $product_crawler->filter('div[class="product-description col-xs-8 col-sm-8"] ul[class="result-list-item-desc-list hidden-xs"]')->html();
                        preg_match_all("'<li>(.*?)</li>'si",$product_description_html, $matches);

                        $product_description = "";
                        $brand = null; //default value for brand, because some products may not have brand

                        foreach ($matches[1] as $match){
                            if(strpos($match, "Brand:") !== false){
                                $brand = $this->crawlBrand($match);
                            } else{
                                $product_description .= $match . ";";
                            }
                        }

                        $product_description = substr($product_description, 0, -1);

                        $product_img_src = $product_crawler->filter('div[class="product-image col-xs-4 col-sm-4"] img')->attr('src');

                        $product_img_content = file_get_contents($product_img_src);

                        $product_img_name = sha1($product_img_content) . ".jpg";

                        $img_directory = public_path() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . $subcategory_name;
                        if(!is_dir($img_directory)){
                            mkdir($img_directory, 0777, true);
                        }

                        $img_filename = public_path() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . $subcategory_name. DIRECTORY_SEPARATOR . $product_img_name;

                        if(!file_exists($img_filename)){
                            file_put_contents($img_filename, $product_img_content);
                        }

                        $isset_product = $product_ctrl->findOneByField('name', $product_name);
                        if($isset_product === null){
                            $product = $product_ctrl->create(array('name' => $product_name, 'price' => $product_price, 'description' => $product_description, 'image_name' => $product_img_name), $category->id, $brand);

                        } else{
                            $product = $isset_product;
                            $product_ctrl->update($product, array('price' => $product_price, 'description' => $product_description, 'image_name' => $product_img_name));
                        }

                        $product->subcategories()->attach($subcategory->id);
                    }

                }

                $page++;
            }
        }
        echo "<pre>";
        print_r("exit");
        die();

    }

    /**
     * @param $category_content
     * @return \App\Category
     */
    public function crawlCategory($category_content){
        $category_ctrl = new CategoryController();
        $category_name = trim($category_content);
        $isset_category = $category_ctrl->findOneByField('name', $category_name);

        if($isset_category === null){ //if category not exists
            $category = $category_ctrl->create(array('name' => $category_name));
        } else{ //if category exists
            $category = $isset_category;
        }

        return $category;
    }

    /**
     * @param $content
     * @return \App\Brand
     */
    public function crawlBrand($content){
        $brand_ctrl = new BrandController();

        $product_brand = trim(str_replace("Brand:", "", $content));

        $isset_brand = $brand_ctrl->findOneByField( 'name', $product_brand);

        if($isset_brand === null){
            $brand = $brand_ctrl->create(array('name' => $product_brand));
        } else{
            $brand = $isset_brand;
        }

        return $brand;
    }
}
