<?php

namespace App\Http\Controllers;

use App\Subcategory;
use App\Wishlist;
use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\App;


class WishlistController extends Controller
{
    CONST WISHLIST_TABLE = 'wishlists';

    public function getWishlistTableName(){
        return self::WISHLIST_TABLE;
    }

    public function findOneByField($field, $value){
        return Wishlist::where($field, $value)->first();
    }

    public function create(Request $request){
        $wishlist = new Wishlist;

        $product_ids = $request->product_ids;
        if(is_array($product_ids)){
            $product_ids = implode(',', $product_ids);
        }
        $wishlist->username = $request->username;
        $wishlist->product_ids = $product_ids;

        $wishlist->save();

        $encoded_key[] = $wishlist->id;
        $encoded_key[] = $wishlist->username;
        $encoded_key[] = $wishlist->product_ids;

        $json = json_encode($encoded_key);

        $encoded_key = base64_encode($json);

        $wishlist->encoded_key = $encoded_key;

        $url = App::make('url')->to('/') . '/wishlist/show_shared/' . $encoded_key;

        return $url;
    }

    public function showShared(Request $request){
        $encoded_key = $request->encoded_key;
        $json = base64_decode($encoded_key);

        $data = json_decode($json, true);
        $product_ids = explode(',', $data[2]);
        $products = Product::whereIn('id', $product_ids)->get();

        foreach ($products as $product){ //I put inside the product object, the name of the subcategory. With this I can use the same partial that I'm using in the products view
            $subcategory_name = $product->subcategories->first()->name;
            $product["subcategory_name"] = $subcategory_name;
        }

        return view('wishlist.show_shared')->with(['products' => $products]);
    }
}
