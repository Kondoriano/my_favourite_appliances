<!DOCTYPE html>
<html lang="en">
<head>
    @include('base_layout.partial_head')
</head>
<body>
@include('base_layout.partial_nav_bar')

@yield('content')

@include('base_layout.partial_footer')
 </body>
</html>