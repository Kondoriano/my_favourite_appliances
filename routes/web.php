<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/test', function (){
//   return view('test');
//});
Route::get('crawler/product', 'WebCrawlerController@crawlProduct')->name('crawler.crawlProduct');
Route::get('catalog/subcategories', 'SubcategoryController@getSubcategories')->name('web.subcategories');
Route::get('catalog/products', 'ProductController@getProducts')->name('web.products');

Route::get('wishlist/create', 'WishlistController@create')->name('wishlist.create');
Route::get('wishlist/show_shared/{encoded_key}', 'WishlistController@showShared')->name('wishlist.show_shared');

Route::get('/products', 'WebController@index')->name('web.products');
Route::get('/', 'WebController@index')->name('web.products');

Route::get('test/{id}', function ($id){
   return view('test',['id' => $id]);
});



