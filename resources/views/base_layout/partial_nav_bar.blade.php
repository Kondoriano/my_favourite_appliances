<div class="navbar_horizontal">
    <a href="#home">Home</a>
    <a href="#news">Top Wishlisted</a>
    @isset($categories)
    <div class="dropdown_nav">
        <button class="dropbtn_nav">Categories
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content_nav">
            @foreach($categories as $category)
                <a href="#" onclick="loadSubcategories('{{$category->name}}')"> {{$category->name}}</a>
            @endforeach
        </div>
    </div>
    @endisset
    <div class="dropdown_nav">
           <select class="custom-select" id="select_sort_by" style="margin-top: 5px; display: none" >
               <option selected> - Order By -</option>
               <option value="price_asc">Price low to hight</option>
               <option value="price_desc">Price hight to low</option>
               <option value="name_asc">Title A-Z</option>
               <option value="name_desc">Title Z-A</option>
           </select>
    </div>
    <div class="dropdown_nav" style="float: right; display: none" id="navbar_wishlist">
        <button class="dropbtn_nav" data-toggle="modal" data-target="#wishlist_modal" id="display_modal">Show Wishlist
            <i class="fa fa-caret-down"></i>
        </button>
    </div>
</div>

{{-- START Wishlist modal--}}
<div id="wishlist_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="wishlist_modal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="wishlist_modal">Current Wishlist</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Appliances: </h5>
                <div>
                    <ul id="wish_list_elements">

                    </ul>
                </div>
            </div>
            <div class="modal-footer" id="modal_buttons">
                <input type="text" class="form-control" placeholder="Username" id="wishlist_username" style="width: 30%; float: left">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="saveWishlist()">Save Wishlist</button>
            </div>
            <div class="modal-footer" id="modal_link" style="display: none">
                <span id="wishlist_link"></span>
            </div>
        </div>
    </div>
</div>
{{-- END Wishlist modal--}}

<script>
  function loadSubcategories(category_name) {
        $.ajax({
            method: "GET",
            url: "{{ url('/catalog/subcategories') }}",
            data: {category_name:category_name},
            dataType: 'json',
            success: function (response) {
                $("#subcategories_menu").empty();

                $("#subcategories_menu").append('<a href="#" class="active">' + category_name  + '</a>');
                $.each(response, function (key, value) {
                    $("#subcategories_menu").append('<a href="#" onclick="loadProducts(' + value.id + ')">' + value.name +  '</a>')
                });
                $("#subcategories_menu").show();
            },
            error: function (response) {
                console.log(response);
            }
        });
    }

</script>