@foreach($products as $product)
    @if(isset($product['subcategory_name']))
        @php $subcategory_name = $product['subcategory_name'] @endphp
    @endif
    <div class="col-md-3">
        <div class="card mb-3 shadow-sm">
            <h6 class="card-header">{{$product->name}}</h6>
            <img src="{{asset('images/' . $subcategory_name . '/' . $product->image_name)}}">
            <div class="card-body">
                <p class="card-text">
                    <ul>
                    @foreach(explode(';', $product->description) as $descr_item)
                        <li>{{$descr_item}}</li>
                    @endforeach
                    </ul>

                </p>
                <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-outline-secondary" onclick="addToWishlist('{{$product->id}}','{{$product->name}}','{{$product->price}}')">Add to wishlist</button>
                    </div>
                    <em class="text-muted">{{$product->price}}€</em>
                </div>
            </div>
        </div>
    </div>

@endforeach