@extends('base_layout.base')
@section('content')
    <div class="card mb-3 shadow-sm" style="float: left">
        <div id="subcategories_menu" class="vertical-menu" style="display: none">

        </div>
    </div>
    <div class="container">
        <div class="row" id="products_container">

        </div>
    </div>
    <script>
        let GLOBALVAR_subcategory_id = ''; //Not global at all, just to make it easier to find
        function loadProducts(subcategory_id, order_by = null, order_direction = null) {
            GLOBALVAR_subcategory_id = subcategory_id;

            $.ajax({
                method: "GET",
                url: "{{ url('/catalog/products') }}",
                data: {subcategory_id:subcategory_id, order_by:order_by, order_direction:order_direction},
                dataType: 'html',
                success: function (response) {
                    $('#products_container').html(response);
                    $('#select_sort_by').show();
                },
                error: function (response) {
                    console.log(response);
                }
            })
        }

        var wishlist_list = [];
        function addToWishlist(element_id, element_name, element_price) {
            wishlist_list.push(element_id);
            $('#wish_list_elements').append('<div class="wishlist_element" onclick="removeFromWishlist(' + element_id + ')"><i class="fas fa-times" style="color: red" title="REMOVE"></i>&nbsp;' + element_name + ' (' + element_price +'€)</div>');
            $('#navbar_wishlist').show();
        }

        //To make a new ajax call to refresh the catalog due to a new order
        $('#select_sort_by').on('change', function (event) {
            event.preventDefault();
            var values = $('#select_sort_by option:selected').val();

            // To get the sort field and direction, I separate both values in the select by "_", so then I just need to split
            var sort_elements = values.split('_');

            loadProducts(GLOBALVAR_subcategory_id, sort_elements[0], sort_elements[1]);
        });

        //To remove elements from the modal (wishlist)
        $(document).on('click', '.wishlist_element', function() {
            $(this).remove();
        });

        //To remove element from the array of wishlist
        function removeFromWishlist(element){
            wishlist_list.splice( $.inArray(element.toString(), wishlist_list), 1);
        }


        //This is the default load, not the best approach...
        loadSubcategories('Small Appliances'); //Default category load
        loadProducts(1); //Default subcategory load

        function saveWishlist() {
          var username = $('#wishlist_username').val();
          $.ajax({
            method: "get",
            url: "{{ url('/wishlist/create') }}",
            data: {username:username, product_ids:wishlist_list},
            dataType: 'html',
            success: function (response) {
              $('#wishlist_link').append('<a href="' + response +'" target="_blank">Link to your wishlist</a>');
              $('#modal_buttons').hide();
              $('#modal_link').show();
            },
            error: function (response) {
              console.log(response);
            }
          })
        }

    </script>

@endsection
