<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Subcategory;

class SubcategoryController extends Controller
{
    CONST SUBCATEGORY_TABLE = 'subcategories';

    public function create($params, $category){
        $subcategory = new Subcategory;
        $subcategory->name = $params["name"];

        $subcategory->category()->associate($category->id);

        $subcategory->save();

        return $subcategory;
    }

    public function getSubcategoryTableName(){
        return self::SUBCATEGORY_TABLE;
    }

    public function findOneByField($field, $value){
        return Subcategory::where($field, $value)->first();
    }

    public function getSubcategories(Request $request){
        $category = Category::where('name', $request->category_name)->first();

        $subcategories = $category->subcategories;

        return $subcategories;
    }
}
