@extends('base_layout.base')
@section('content')
    <div class="container">
        <div class="row" id="products_container">
            @include('partials._products_catalog')
        </div>
    </div>

@endsection